aws cloudformation package \
--template-file parent.template.yml \
--s3-bucket applying-bucket-templates \
--output-template-file parent.yml

aws cloudformation create-stack --stack-name demo-25   \
--template-body file://template.yaml \
--parameters ParameterKey=CreateResources,ParameterValue=SI  \
ParameterKey=InstanceType,ParameterValue='t3.nano'   \
ParameterKey=KeyName,ParameterValue='demov1'   \
ParameterKey=SSHLocation,ParameterValue='0.0.0.0/0'
